import React, { lazy } from 'react';
import Cookies from 'js-cookie';
import PageHeader from 'components/PageHeader';
import SideNav from 'components/SideNav';
import { Redirect, Route, Switch } from 'react-router-dom';
import styles from './styles.module.scss';
import { useQuery } from 'react-query';
import { loadProfile } from 'api/profile';

const Tasks = lazy(() => import('pages/Tasks'));
const Chats = lazy(() => import('pages/Chats'));
const Users = lazy(() => import('pages/Users'));
const Product = lazy(() => import('pages/Product'));

export default function PageWrapper() {
  const isAuthenticated = !!Cookies.get('token');
  const { data: profile } = useQuery('profile', loadProfile, { enabled: isAuthenticated });

  if (!isAuthenticated) return <Redirect to="/login" />;
  if (!profile) return null;
  return (
    <div className={styles.pageWrapper}>
      <PageHeader />
      <div className={styles.mainWrapper}>
        <SideNav />
        <div className={styles.pageContent}>
          <Switch>
            <Route exact path="/tasks" component={Tasks} />
            <Route exact path="/chats/:chatRoomId" component={Chats} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/products" component={Product} />
          </Switch>
        </div>
      </div>
    </div>
  );
}
