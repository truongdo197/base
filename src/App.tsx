import React, { Suspense } from 'react';
import { createBrowserHistory } from 'history';
import RootWrapper from './wrappers/RootWrapper';
import { Router } from 'react-router-dom';
import { ReactQueryCacheProvider, QueryCache } from 'react-query';
import { ReactQueryDevtools } from 'react-query-devtools';
import configs from 'config';
import socket from 'socket';

export const history = createBrowserHistory();
const queryCache = new QueryCache({
  defaultConfig: {
    queries: {
      refetchOnWindowFocus: false,
      cacheTime: 24 * 3600 * 1000, // cache for 1 day
    },
  },
});

function App() {
  socket.on('connect', function (data: any) {
    socket.emit('join', 'hello server from client');
  });
  return (
    <ReactQueryCacheProvider queryCache={queryCache}>
      <Router history={history}>
        <Suspense fallback={null}>
          <RootWrapper />
          {configs.APP_ENV !== 'prod' && <ReactQueryDevtools initialIsOpen />}
        </Suspense>
      </Router>
    </ReactQueryCacheProvider>
  );
}

export default App;
