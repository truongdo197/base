import { sendGet } from './axios';

// eslint-disable-next-line import/prefer-default-export
export const getListProduct = (params: any) => sendGet('/v1/app/product/', params);
