import { sendGet } from './axios';

// eslint-disable-next-line import/prefer-default-export
export const getListUsers = () => sendGet('/v1/app/users/get-list');
