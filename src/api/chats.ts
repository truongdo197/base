import { sendGet, sendPut, sendPost } from './axios';

export const getChatRoom = (receiverId: any) => sendPut('/v1/app/chats/get-chat-room', receiverId);

export const sendMessage = (params: any) => sendPost('/v1/app/chats/add-message', params);

export const getMessage = (chatRoomId: any) => sendPut(`/v1/app/chats/get-all-message-chat-room/${chatRoomId}`);
