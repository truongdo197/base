import { Button, Pagination, Row } from 'antd';
import { getListProduct } from 'api/product';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import ProductList from './ProductList';

const pageSize = 5;

export default function Product() {
  const [filter, setFilter] = useState({ pageIndex: 1, pageSize });
  const { data, isFetching, refetch } = useQuery('products', () => getListProduct(filter));
  const handlePageChange = (page: number) => setFilter((oldFilter) => ({ ...oldFilter, pageIndex: page }));
  console.log('data', data);
  return (
    <div>
      <Row justify="space-between">
        <h2>List of Task</h2>
        <Button onClick={() => refetch()}>Reload</Button>
      </Row>
      <ProductList loading={isFetching} projects={data?.data} />
      <p />
      <Pagination
        current={data?.pageIndex}
        total={data?.totalItems}
        pageSize={pageSize}
        onChange={handlePageChange}
        showSizeChanger={false}
      />
    </div>
  );
}
