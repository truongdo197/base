import React from 'react';
import { Table, Button } from 'antd';

interface IProps {
  projects: any;
  loading: boolean;
}

const ProductList = ({ projects, loading }: IProps) => {
  const columns = [
    {
      title: 'name',
      dataIndex: 'name',
    },
    {
      title: 'description',
      dataIndex: 'description',
    },
    {
      title: 'Outlet',
      dataIndex: 'outlet',
    },
    {
      title: 'Actions',
      render: (text: any, record: any) => {
        return (
          <>
            <Button>Detail</Button>
            &nbsp;
            <Button>Edit</Button>
          </>
        );
      },
    },
  ];
  return <Table rowKey="id" dataSource={projects || []} columns={columns} pagination={false} loading={loading} />;
};

export default ProductList;
