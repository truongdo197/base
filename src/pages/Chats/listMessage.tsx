import { getMessage } from 'api/chats';
import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import socket from 'socket';

export default function Messages({ chatRoomId }: any) {
  const { data, isFetching, refetch } = useQuery(['message'], () => getMessage(chatRoomId));
  const [messages, setMessages] = useState([]) as any;

  useEffect(() => {
    socket.on('newmessage', (data: any) => {
      setMessages([...messages, { ...data }]);
      console.log(data);
    });
  });
  console.log('messages', messages);
  console.log(data);
  return <div>{messages && messages.map((item: any, index: any) => <div key={index}>{item.content}</div>)}</div>;
}
