import { Button, Col, Row } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { sendMessage, getChatRoom, getMessage } from 'api/chats';
import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import socket from 'socket';
import { useJwt } from 'react-jwt';
import Cookies from 'js-cookie';
import Messages from './listMessage';

const pageSize = 5;

export default function Chats() {
  const { data, isFetching, refetch } = useQuery(['message'], () => getMessage(params.chatRoomId));
  const [textMessage, setTextMessage] = useState('');
  const [messages, setMessages] = useState([]) as any;
  const [message, setMessage] = useState();
  const params = useParams() as { chatRoomId: '' };
  const history = useHistory();

  let token = Cookies.get('token');
  const { decodedToken } = useJwt(String(token));

  const receiverId = Object(history.location.state);

  const onTextChange = (e: any) => {
    setTextMessage(e.target.value);
  };

  const sendMessages = async () => {
    const data = {
      senderId: decodedToken.id,
      ...receiverId,
      chatRoomId: Number(params.chatRoomId),
      content: textMessage,
      type: 1,
    };
    socket.emit('message', data);

    socket.on('newmessage', (data: any) => {
      setMessages([...messages, { ...data }]);
    });

    // await sendMessage(data).then((response) => {
    //   const { data } = response;
    //   if (data) {
    //     setTextMessage('');
    //   }
    // });
  };

  console.log(messages);
  // useEffect(() => {
  //   socket.on('newmessage', (data: any) => {
  //     console.log('data', data);
  //   });
  // });

  return (
    <div>
      <Row>
        <Col xs={12}>
          <Messages chatRoomId={params.chatRoomId} />
          <TextArea name="message" rows={3} onChange={onTextChange} />
        </Col>
        <Col xs={24}>
          <Button type="primary" className="btnCustome" style={{ marginTop: 10 }} onClick={sendMessages}>
            Gửi
          </Button>
        </Col>
      </Row>
    </div>
  );
}
