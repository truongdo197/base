import React, { useState } from 'react';
import { Pagination, Button, Row } from 'antd';
import { useQuery } from 'react-query';
import TaskList from './TaskList';
import { getListTask } from 'api/task';

const pageSize = 5;

export default function Tasks() {
  const [filter, setFilter] = useState({ pageIndex: 1, pageSize });

  const { data, isFetching, refetch } = useQuery(['task', filter], () => getListTask(filter));
  const handlePageChange = (page: number) => setFilter((oldFilter) => ({ ...oldFilter, pageIndex: page }));

  return (
    <div>
      <Row justify="space-between">
        <h2>List of Task</h2>
        <Button onClick={() => refetch()}>Reload</Button>
      </Row>
      <TaskList loading={isFetching} projects={data?.data} />
      <p />
      <Pagination
        current={data?.pageIndex}
        total={data?.totalItems}
        pageSize={pageSize}
        onChange={handlePageChange}
        showSizeChanger={false}
      />
    </div>
  );
}
