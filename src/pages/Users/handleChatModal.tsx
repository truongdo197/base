import Modal from 'antd/lib/modal/Modal';
import { getChatRoom } from 'api/chats';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

interface IProps {
  data: any;
  isVisible: boolean;
  setIsVisible: any;
}

const HandleChatModal = ({ data, isVisible, setIsVisible }: IProps) => {
  const history = useHistory();
  const handleOk = () => {
    const receiverId = { receiverId: Number(data?.id) };
    getChatRoom(receiverId).then((response) => {
      const { data } = response;
      history.push({ pathname: `/chats/${data.id}`, state: { receiverId: data.id } });
      setIsVisible(false);
    });
  };

  const handleCancel = () => {
    setIsVisible(false);
  };

  return (
    <div>
      <Modal title="Basic Modal" visible={isVisible} onOk={handleOk} onCancel={handleCancel}>
        <div>
          <p>Do you want to chat with {data?.username}</p>
        </div>
      </Modal>
    </div>
  );
};
export default HandleChatModal;
