import { Pagination } from 'antd';
import { getListUsers } from 'api/user';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import UserList from './UserList';

const pageSize = 5;

export default function Users() {
  const [filter, setFilter] = useState({ pageIndex: 1, pageSize });
  const { data, isFetching, refetch } = useQuery(['user'], () => getListUsers());
  const handlePageChange = (page: number) => setFilter((oldFilter) => ({ ...oldFilter, pageIndex: page }));

  return (
    <div>
      <h2>List of User</h2>
      <UserList loading={isFetching} projects={data?.data} />
      <Pagination
        current={data?.pageIndex}
        total={data?.totalItems}
        pageSize={pageSize}
        onChange={handlePageChange}
        showSizeChanger={false}
      />
    </div>
  );
}
