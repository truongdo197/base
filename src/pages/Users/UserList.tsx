import React, { useEffect, useState } from 'react';
import { Table, Button } from 'antd';
import HandleChatModal from './handleChatModal';

interface IProps {
  projects: any;
  loading: boolean;
}

const UserList = ({ projects, loading }: IProps) => {
  const [isVisible, setIsVisible] = useState(false);
  const [data, setData] = useState();
  useEffect(() => {
    setData(projects);
  }, [projects]);

  const handleChat = (e: any) => {
    setData(e);
    setIsVisible(true);
  };
  const columns = [
    {
      title: 'username',
      dataIndex: 'username',
    },
    {
      title: 'Actions',
      render: (text: any, record: any) => {
        return (
          <>
            <Button onClick={() => handleChat(record)}>Chat</Button>
          </>
        );
      },
    },
  ];

  return (
    <div>
      <HandleChatModal data={data} isVisible={isVisible} setIsVisible={setIsVisible} />
      <Table rowKey="id" dataSource={projects || []} columns={columns} pagination={false} loading={loading} />
    </div>
  );
};

export default UserList;
