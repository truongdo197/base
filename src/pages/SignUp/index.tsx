import React from 'react';
import { useHistory } from 'react-router-dom';
import styles from './style.module.scss';
import { Card, Input, Button, Form, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import Cookies from 'js-cookie';
import _ from 'lodash';
import { signUp } from 'api/authentication';
import { handleErrorMessage } from 'helper';

export default function SignUp() {
  const history = useHistory();
  const { t } = useTranslation();

  const navigateToLogIn = () => history.push('/login');

  const handleSubmit = async (payload: any) => {
    const params = _.pick(payload, ['username', 'password']);
    try {
      const data = await signUp(params);
      const { token, refreshToken } = data.data;
      Cookies.set('token', token, {
        expires: payload.rememberMe ? 999999 : undefined,
      });
      Cookies.set('refreshToken', refreshToken, {
        expires: payload.rememberMe ? 999999 : undefined,
      });
      history.push('/');
    } catch (error) {
      handleErrorMessage(error);
    }
  };

  return (
    <div className={styles.signUpContainer}>
      <Card bordered className={styles.signUpForm}>
        <Form onFinish={handleSubmit}>
          <Row justify="center">
            <h2>Đăng ký</h2>
          </Row>
          <Form.Item
            label="Tên đăng nhập"
            name="username"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập',
              },
            ]}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[{ required: true, message: t('validate.passwordRequired') }]}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="Xác nhận mật khẩu"
            name="passwordConfirm"
            dependencies={['password']}
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject('The two passwords that you entered do not match!');
                },
              }),
            ]}
            labelAlign="left"
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button block type="primary" htmlType="submit">
              Đăng ký
            </Button>
          </Form.Item>
          <Form.Item labelCol={{ span: 24 }}>
            <Button block type="dashed" htmlType="button" onClick={navigateToLogIn}>
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}
