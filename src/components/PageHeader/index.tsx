import React from 'react';
import Cookies from 'js-cookie';
import avatarImg from 'assets/images/avatar.svg';
import { Link, useHistory } from 'react-router-dom';
import styles from './styles.module.scss';
import { Menu, Dropdown } from 'antd';
import { useQuery } from 'react-query';

export default function PageHeader() {
  const history = useHistory();
  const { data }: { data: any } = useQuery('profile');
  const profile = data?.data;

  const handleLogout = () => {
    Cookies.remove('token');
    Cookies.remove('refreshToken');
    history.push('/login');
  };

  const menu = (
    <Menu style={{ minWidth: 200 }}>
      <Menu.Item key="1">Profile</Menu.Item>
      <Menu.Item key="2">Change Password</Menu.Item>
      <Menu.Item key="3" onClick={handleLogout}>
        Logout
      </Menu.Item>
    </Menu>
  );

  return (
    <div className={styles.headerWrapper}>
      <Link className={styles.logo} to="/index">
        HOME
      </Link>
      <div className={styles.menuWrapper}>
        <div className={styles.menuItem}>
          <Dropdown overlay={menu} trigger={['click']}>
            <div>
              <span>{`Hi ${profile?.fullName || profile?.username}!`}</span>
              &nbsp;
              <img className={styles.icon} src={avatarImg} alt="" />
            </div>
          </Dropdown>
        </div>
      </div>
    </div>
  );
}
